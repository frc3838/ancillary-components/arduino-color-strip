
// This version modified by Mitch Burberry from various routines freely available on line
// Apologies for not properly referencing all the sources
// This program continuously runs a selected NEO_Pixel color pattern
// There is a case statement to choose the Show patern however
// In this version the choice is forced to Show 3 to display distance dependent color patterns.  
// Other Color pattern "Shows" include; 
// 3838 team green chased by blue
// 3838 team green chased by red
// rainbow cyles
// brightness pulsing 3838 green breathDelay slows down, breath rate speeds up
// white Theater Chase
// Theater Chase Rainbow
// all red 
// all green
// all blue
// all cyan
// all magenta
// all yellow
// Shows run in infinit loops.
// Turn off lights by removing the fuse that powers the Metro and Neo_pixel strip
// Use a jumper wire between ground and an anolog pin from A0 to A5 on the Arduino Metro to select a color pattern.
// The jumper selects one of the first 6 shows *** rearrange the case statement to load different patterns
// There is code for the ultrasonic detector to measure distance and set a distance dependent color pattern 
// But this can be ignored when the detector is not present.
// No external button is needed to select shows and the Button_pin is not used in this version. 
// Progress messages are sent to the monitor when the arduino metro is connected to a computer and monitor initiated.


#include <Adafruit_NeoPixel.h>  // This is the library needed to run the NeoPixel routines

#define BUTTON_PIN   2    // Digital IO pin connected to the external button.  *** not used in this version
// Not used               // button driven with a pull-up resistor so the switch should
// No Button              // pull the pin to ground momentarily.  On a high -> low
// in this version        // transition the button press logic will execute.

// Ultrasonic pin outs on Arduino Metro 
#define echoPin 7 // Echo Pin
#define trigPin 6 // Trigger Pin

#define PIXEL_PIN    5  // Digital IO pin connected to data line on the NeoPixel LED strip

#define PIXEL_COUNT 32  // number of LEDs on the NeoPixel strip




Adafruit_NeoPixel strip = Adafruit_NeoPixel(PIXEL_COUNT, PIXEL_PIN, NEO_GRB + NEO_KHZ800);
// Parameter 1 = number of pixels in strip
// Parameter 2 = pin number (most are valid)
// Parameter 3 = pixel type flags, add together as needed:
//   NEO_RGB     Pixels are wired for RGB bitstream
//   NEO_GRB     Pixels are wired for GRB bitstream, correct for neopixel stick
//   NEO_KHZ400  400 KHz bitstream (e.g. FLORA pixels)
//   NEO_KHZ800  800 KHz bitstream (e.g. High Density LED strip), correct for neopixel stick



bool oldState = HIGH;

int showType = 1;      // ***** set default here ******* showType will be selected from jumper wire and case list ... 

int stripDelay = 0;          // time to wait in (ms)
int breathDelay = 5;         // time to wait in (ms)
int breathRate = 16;         // Factor to speed up breathing rate
int sweepRepeat = 20;        // not used ... uses button state instead.
int numberShows = 18;        // total number of shows in case list
int contactSignal = 13;      // use pin 13 with LED on Metro to Indicate contact = on
int contactSignal2 = 12;     // use pin 12 for duplicate contact signal for use with PWM cable  (cut red wire and use black grn (14) and white signal (on 12)
long duration, distance;     // Duration used to calculate distance  
float distanceInches;        // distance in inches
int distanceLED;             // distance in LED spacing
float aPin[6] = {0,0,0,0,0,0};         // analog pins init

void setup() {
  Serial.begin(9600);
  pinMode(BUTTON_PIN, INPUT_PULLUP);
  pinMode(contactSignal, OUTPUT);  // use LED on board Arduino for visual reference
  pinMode(contactSignal2, OUTPUT);  // use with white lead on PWM cable
  pinMode(A0, INPUT);   //Pin will read HIGH unless shorted to ground
  pinMode(A1, INPUT);   //Pin will read HIGH unless shorted to ground
  pinMode(A2, INPUT);   //Pin will read HIGH unless shorted to ground
  pinMode(A3, INPUT);   //Pin will read HIGH unless shorted to ground
  pinMode(A4, INPUT);   //Pin will read HIGH unless shorted to ground
  pinMode(A5, INPUT);   //Pin will read HIGH unless shorted to ground
  // note: choose light mode by connecting one of the inputs above to ground
  
  strip.begin();    // Initialize NeoPixel strips
  strip.show();     // Set all pixels to 'off'
  
  Serial.println("Program to Demonstrate LED Patterns");
  
  
  // ultrasonic pin setup
  digitalWrite(contactSignal, LOW);   // initially assume no contact.
  digitalWrite(contactSignal2, LOW);  // initially assume no contact.
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  
}

void loop() {
  
   // ************  set show type here ************
        // Determin which mode pin is selected
        
 for (int i = 0; i < 6; i++){
  aPin [i]= analogRead(i);
  // Serial.println( aPin [i]);
  if (aPin[i] < 1) showType = i;
 }
 // Comment out the next line if you want the jumper selection above to choose the showType
  showType = 3; //  Force show to be 3: UltrasonicDistanceWallDetector(stripDelay);  
  //Serial.print("ShowType ");
  //Serial.println( showType);
  
   startShow(showType);

}

void startShow(int i) {
 
  switch(i){
    case 0: White(stripDelay);    // White
            Serial.print(" case 0: ColorBurst");
  //          Serial.println(", Display Completed");
            break;
    case 1: Green(stripDelay); //Green
            Serial.print("case 1: TeamWipe");
  //          Serial.println(", Display Completed");
            break;
    case 2: Red(stripDelay); //Red
            Serial.print("case 2: TeamWipe");
  //          Serial.println(", Display Completed");
            break;
    case 3: UltrasonicDistanceWallDetector(stripDelay);    // use LED as output for Wall Proximity
            //Serial.print(" case 3: UltrasonicDistanceWallDetector");
            //Serial.println(", Display Completed");
            break;  
    case 4: UltrasonicDistance(stripDelay);    // use LED as output for distance 
            Serial.print(" case 4: UltrasonicDistance");
 //           Serial.println(", Display Completed");
            break;  
    case 5: Off(stripDelay);    //Off
            Serial.print("case 5: off");
 //           Serial.println(", Display Completed");
            break; 
  
        }
}


void Off(uint8_t wait ) {
   colorWipe(strip.Color(0, 0, 0), 4);                   // turn off the LED strip
}

void White(uint8_t wait ) {
      colorWipe(strip.Color(127, 127, 127), wait);    // White
}

void Red(uint8_t wait ) {
      colorWipe(strip.Color(127, 0, 0), wait);    // Red
}

void Green(uint8_t wait ) {
      colorWipe(strip.Color(0, 127, 0), wait);    // Green
}

void Blue(uint8_t wait ) {
      colorWipe(strip.Color(0, 0, 127), wait);    // Blue
}

void Cyan(uint8_t wait ) {
      colorWipe(strip.Color(0,127, 127), wait);    // Cyan
}      

void Magenta(uint8_t wait ) {
      colorWipe(strip.Color(127, 0, 127), wait);    // Magenta
} 
  
void Yellow(uint8_t wait ) {
      colorWipe(strip.Color(127, 127, 0), wait);    // Yellow
}  

void UltrasonicDistance(uint8_t wait ) {
    
    bool newState = HIGH;
    
   do {
     
  /* The following trigPin/echoPin cycle is used to determine the
 distance of the nearest object by bouncing soundwaves off of it. */ 
 digitalWrite(trigPin, LOW); 
 delayMicroseconds(2); 
 digitalWrite(trigPin, HIGH);
 delayMicroseconds(10); 
 digitalWrite(trigPin, LOW);
 duration = pulseIn(echoPin, HIGH);
 
 //Calculate the distance (in cm) based on the speed of sound.
 distance = duration / 58.2; // add 1 to calibrate offset.
 distanceInches = distance / 2.54;
 distanceLED  = distanceInches  / 1.25;
 
//Serial.println(distanceLED);
//delay(wait/2);  
    
    // Check if state changed from high to low (button press).
    newState = LOW;
    
  }while (newState == HIGH);
  // turnOffStrip();                    // turn off the LED strip
  
}


void UltrasonicDistanceWallDetector(uint8_t wait ) {
    
    bool newState = HIGH;
    
   do {
     
  /* The following trigPin/echoPin cycle is used to determine the
 distance of the nearest object by bouncing soundwaves off of it. */ 
 digitalWrite(trigPin, LOW); 
 delayMicroseconds(2); 
 digitalWrite(trigPin, HIGH);
 delayMicroseconds(10); 
 digitalWrite(trigPin, LOW);
 duration = pulseIn(echoPin, HIGH);
 
 //Calculate the distance (in cm) based on the speed of sound.
 //distance = 1 + duration / 58.2; // add 1 to calibrate offset.
 distance = duration / 58.2;
 distanceInches = distance / 2.54;
 distanceLED  = distanceInches  / 1.25;
  Serial.print(distanceInches);
  Serial.println(" in");
 if (distanceInches < 6) {
    Green(1);    // Green
    digitalWrite(contactSignal, HIGH);  // Signal "In Contact"
    digitalWrite(contactSignal2,HIGH);  // Signal "In Contact"
 }
 else {
   if(distanceInches >24) {
     White(1);    // White
     digitalWrite(contactSignal, LOW);  // Signal "No Contact"
     digitalWrite(contactSignal2, LOW); // Signal "No Contact"
   }
   else {
   if(distanceInches <12) {
     Red(1);    // Red
     digitalWrite(contactSignal, LOW);  // Signal "No Contact"
     digitalWrite(contactSignal2, LOW); // Signal "No Contact"
   }
  else{
   Blue(1);    // Blue
   digitalWrite(contactSignal, LOW);  // Signal "No Contact"
   digitalWrite(contactSignal2, LOW); // Signal "No Contact"
   
 //  stripLEDMeter(strip.Color(127, 0, 0), distanceInches , stripDelay);        // Red indicator
//   strip.show();
 }}}
 // stripLEDMeter(strip.Color(127, 0, 0), distanceInches , stripDelay);        // Red indicator
 strip.show();


 delay(wait/2);  
    
    // Check if state changed from high to low (button press).
    newState =LOW;
    
  }while (newState == HIGH);
  // turnOffStrip();                    // turn off the LED strip
  
}
// Fill the dots one after the other with a color
void colorWipe(uint32_t c, uint8_t wait) {
  for(uint16_t i=0; i<strip.numPixels(); i++) {
    strip.setPixelColor(i, c);
    strip.show();
    delay(wait);
  }
}

// Turn strip off
void turnOffStrip() {
  
 turnOffStrip();              // turn off the LED strip
                            
}
void stripLEDMeter(uint32_t c1, int illuminationLength, uint8_t wait) {

  if (illuminationLength > PIXEL_COUNT) {
    illuminationLength = PIXEL_COUNT;
  }
     for(uint16_t i=illuminationLength; i<PIXEL_COUNT; i++) {
    strip.setPixelColor(i, strip.Color(0, 0, 0));
    strip.show();
    delay(wait);
  }
   for(uint16_t i=0; i<illuminationLength; i++) {
    strip.setPixelColor(i, c1);
    strip.show();
    delay(wait);
  }
  
}
